## Instructions

Use the example-dataset.ipynb notebook to download the data from source and upload it to AWS's storage system S3. 

To use the notebook you will have to select a kernel when you open it. Select Python (data science). 

Next select the compute limit you will need for the notebook. Now you're set to go!


When you have created your own AutoML experiments using this data, why not try your hand at a kaggle competition. We suggest https://www.kaggle.com/c/house-prices-advanced-regression-techniques as a good starting point! 